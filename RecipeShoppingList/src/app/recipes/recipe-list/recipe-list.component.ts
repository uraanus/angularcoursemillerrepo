import { Component, OnInit,Output ,EventEmitter} from '@angular/core';
import { Recipe } from '../recipe.model';


@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  @Output() recipeWasSelected=new EventEmitter<Recipe>();
  recipes:Recipe[]=[
    new Recipe('A test Recipe',"This is a simply test",'https://www.cbc.ca/food/content/images/recipes/WinterVegPie.jpg'),
    new Recipe('A tes2 2Recipe',"Pizza Pizza",'https://www.cbc.ca/food/content/images/recipes/WinterVegPie.jpg')
  ];
  constructor() { }

  ngOnInit() {
  }

  onRecipeSelected(recipe : Recipe){
    this.recipeWasSelected.emit(recipe);
  }
}
