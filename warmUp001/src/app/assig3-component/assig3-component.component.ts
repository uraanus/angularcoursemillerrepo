import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-assig3-component',
  templateUrl: './assig3-component.component.html',
  styleUrls: ['./assig3-component.component.css']
})
export class Assig3ComponentComponent implements OnInit {

  dispalyP=false;
  secretArr=[];
  constructor() { }

  onToggleDisplay(){
    this.dispalyP = !this.dispalyP;
    this.secretArr.push(new Date());
  }
  ngOnInit() {
  }
  getDisplayP(){

    return this.dispalyP;
  }

}
 