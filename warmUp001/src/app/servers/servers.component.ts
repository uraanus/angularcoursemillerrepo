import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {

  allowNewServer =false;
  serverCreationStatus='No Servers was created !';
  serverName='TestServer';
  userName = '';
  servercreated=false;

  servers=['Test Servers','Test Servers2']
  constructor() { 

    setTimeout(() => {
      this.allowNewServer=true;
    }, 2000);

  
  }

  onCreateServer(){

    this.servercreated=true;
    this.servers.push(this.serverName);
    this.serverCreationStatus='Server Was Created ! The name is '+ this.serverName ;
  }
  ngOnInit() {
  }

  ouUpdateServerName(event : Event){
    this.serverName=(<HTMLInputElement>event.target).value;
  }
  resetUserName(){
    this.userName='';
  }
}
